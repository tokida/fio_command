#!/bin/sh

TMPDIR=/tmp
HOSTNAME=`hostname`
DATE=`date +"%Y%m%d%H%M%S"`
LOG_POST=${HOSTNAME}_${DATE}

fio -filename=${TMPDIR}/test2g -direct=1 -rw=read -bs=4k -size=2G -numjobs=64 -runtime=10 -group_reporting -name=file1 | tee -a read_${LOG_POST}.log
fio -filename=${TMPDIR}/test2g -direct=1 -rw=write -bs=4k -size=2G -numjobs=64 -runtime=10 -group_reporting -name=file1 | tee -a write_${LOG_POST}.log
fio -filename=${TMPDIR}/test2g -direct=1 -rw=randread -bs=4k -size=2G -numjobs=64 -runtime=10 -group_reporting -name=file1 | tee -a randread_${LOG_POST}.log
fio -filename=${TMPDIR}/test2g -direct=1 -rw=randwrite -bs=4k -size=2G -numjobs=64 -runtime=10 -group_reporting -name=file1 | tee -a randwrite_${LOG_POST}.log

fio -filename=${TMPDIR}/test2g -direct=1 -rw=read -bs=32m -size=2G -numjobs=16 -runtime=10 -group_reporting -name=file1 | tee -a readb_${LOG_POST}.log
fio -filename=${TMPDIR}/test2g -direct=1 -rw=write -bs=32m -size=2G -numjobs=16 -runtime=10 -group_reporting -name=file1 | tee -a writeb_${LOG_POST}.log

rm ${TMPDIR}/test2g


