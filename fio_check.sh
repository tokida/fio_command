#!/bin/sh


READ=`cat read_*.log | grep iops | awk -F= '{print$4}' | awk '{print$1}'`
WRITE=`cat write_*.log | grep iops | awk -F= '{print$4}' | awk '{print$1}'`
RANDREAD=`cat randread_*.log | grep iops | awk -F= '{print$4}' | awk '{print$1}'`
RANDWRITE=`cat randwrite_*.log | grep iops | awk -F= '{print$4}' | awk '{print$1}'`

READB=`cat readb_*.log | grep "bw (KB" | awk -F= '{print$3}' | awk -F, '{print$1}'`
WRITEB=`cat writeb_*.log | grep "bw (KB" | awk -F= '{print$3}' | awk -F, '{print$1}'`


echo "IOPS (io/sec)"
echo "--------------------------------"
echo "Sequential read  : ${READ}"
echo "Sequential write : ${WRITE}"
echo "Randam read      : ${RANDREAD}"
echo "Randam write     : ${RANDWRITE}"

echo ""
echo "BandWide (KB/sec)"
echo "--------------------------------"
echo "Read             : ${READB}"
echo "Write            : ${WRITEB}"

